import yaml
import sys
import json
import requests

def json_escape(md):
    markdown=json.dumps(md)
    return markdown

dev_api_key = sys.argv[1]

with open('articles/articles.yml') as f:
    dict = yaml.safe_load(f)

title = dict['recent_articles'][0]['title']
body = dict['recent_articles'][0]['body']
t = dict['recent_articles'][0]['tags'].split()

f = open(body, 'r')
md = f.read()
markdown = json_escape(md)

tags="["
for i in t:
    tags+= '"' + i + '"'
    if i != t[-1]:
        tags+=","
tags+="]"

url = 'https://dev.to/api/articles'

headers = {'Content-Type': 'application/json', 'api-key': dev_api_key}

json='{"article":{"title":"' + title + '","body_markdown":' + markdown + ',"published":true,"tags":' + tags + '}}'

r = requests.post(url, headers=headers, data=json)

print(r.json()['url'])
