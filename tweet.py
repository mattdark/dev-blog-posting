from twitter import *
import sys
import yaml

with open('articles/articles.yml') as f:
    # use safe_load instead load
    dict = yaml.safe_load(f)

title = dict['recent_articles'][0]['title']
url = sys.argv[5]

t = Twitter(auth=OAuth(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]))

update = "New Blog Post: " + title + "\n" + url + "\n #DEVCommunity"

# Update your status
t.statuses.update(status=update)
